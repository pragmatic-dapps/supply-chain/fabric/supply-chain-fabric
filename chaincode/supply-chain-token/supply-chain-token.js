'use strict';
const shim = require('fabric-shim');
const util = require('util');
const prefix = 'wallet_';

let Chaincode = class {

  async Init(stub) {
    console.info('=========== Instantiated Token chaincode ===========');
    return shim.success();
  }

  async Invoke(stub) {
    let ret = stub.getFunctionAndParameters();
    console.info(ret);

    let method = this[ret.fcn];
    if (!method) {
      console.error('no function of name:' + ret.fcn + ' found');
      throw new Error('Received unknown function ' + ret.fcn + ' invocation');
    }
    try {
      let payload = await method(stub, ret.params);
      return shim.success(payload);
    } catch (err) {
      console.log(err);
      return shim.error(err);
    }
  }
  async initLedger(stub, args) {
    console.info('============= START : Initialize Ledger ===========');
    const walletName = prefix + 'user1';
    const wallet = {
      docType: 'wallet',
      balance: '1000'
    };

    await stub.putState(walletName, Buffer.from(JSON.stringify(wallet)));
    console.info('============= END : Initialize Ledger ===========');
  }

  async createWallet(stub, args) {
    console.info('============= START : Create Wallet ===========');
    if (args.length != 2) {
      throw new Error('Incorrect number of arguments. Expecting 2');
    }
    const walletName = prefix + args[0].toString()
    const balance = args[1].toString();
    const waletAsBytes = await stub.getState(walletName);
    if (waletAsBytes.toString().length > 0) {
      throw new Error(walletName + ' already exists. Value:' + waletAsBytes + '. String value: ' + waletAsBytes.toString());
    }

    const wallet = {
      docType: 'wallet',
      balance: balance
    };

    await stub.putState(walletName, Buffer.from(JSON.stringify(wallet)));
    console.info('============= END : Create wallet ===========');
  }

  async addFundsToWallet(stub, args) {
    console.info('============= START : Add Funds to Wallet ===========');
    if (args.length != 2) {
      throw new Error('Incorrect number of arguments. Expecting 2');
    }
    const walletName = prefix + args[0].toString();
    const funds = args[1].toString();

    let waletAsBytes = await stub.getState(walletName);
    if (!waletAsBytes || waletAsBytes.toString().length <= 0) {
      throw new Error(walletName + ' does not exist: ');
    }
    const wallet = JSON.parse(waletAsBytes.toString());
    wallet.balance = (Number(wallet.balance) + Number(funds)).toString();

    await stub.putState(walletName, Buffer.from(JSON.stringify(wallet)));

    console.info('============= END : Add Funds to Wallet ===========');
  }

  async transferFunds(stub, args) {
    console.info('============= START : Transfer funds ===========');
    if (args.length != 3) {
      throw new Error('Incorrect number of arguments. Expecting 3');
    }
    const fromWalletName = prefix + args[0].toString();
    const toWalletName = prefix + args[1].toString();
    const amount = args[2].toString();

    const fromWaletAsBytes = await stub.getState(fromWalletName);
    if (!fromWaletAsBytes || fromWaletAsBytes.toString().length <= 0) {
      throw new Error(fromWalletName + ' does not exist: ');
    }

    const toWaletAsBytes = await stub.getState(toWalletName);
    if (!toWaletAsBytes || toWaletAsBytes.toString().length <= 0) {
      throw new Error(toWalletName + ' does not exist: ');
    }

    const fromWallet = JSON.parse(fromWaletAsBytes.toString());
    const toWallet = JSON.parse(toWaletAsBytes.toString());

    if(Number(fromWallet.balance) < Number(amount)){
      throw new Error(fromWalletName + ' does not have enough funds.');
    }

    fromWallet.balance = (Number(fromWallet.balance) - Number(amount)).toString();
    toWallet.balance = (Number(toWallet.balance) + Number(amount)).toString();

    await stub.putState(fromWalletName, Buffer.from(JSON.stringify(fromWallet)));
    await stub.putState(toWalletName, Buffer.from(JSON.stringify(toWallet)));

    console.info('============= END : Transfer fundsTransfer fundsTransfer fundsTransfer funds ===========');
  }

  async getWallet(stub, args) {
    if (args.length != 1) {
      throw new Error('Incorrect number of arguments. Expecting 1');
    }
    let walletName = prefix + args[0].toString();

    let waletAsBytes = await stub.getState(walletName); //get the wallet from chaincode state
    if (!waletAsBytes || waletAsBytes.toString().length <= 0) {
      throw new Error(walletName + ' does not exist: ');
    }
    console.log(waletAsBytes.toString());
    return waletAsBytes;
  }

  async queryAllWallets(stub, args) {

    let startKey = '';
    let endKey = '';

    let iterator = await stub.getStateByRange(startKey, endKey);

    let allResults = [];
    while (true) {
      let res = await iterator.next();

      if (res.value && res.value.value.toString()) {
        let jsonRes = {};
        console.log(res.value.value.toString('utf8'));

        jsonRes.Key = res.value.key;
        try {
          jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
        } catch (err) {
          console.log(err);
          jsonRes.Record = res.value.value.toString('utf8');
        }
        allResults.push(jsonRes);
      }
      if (res.done) {
        console.log('end of data');
        await iterator.close();
        console.info(allResults);
        return Buffer.from(JSON.stringify(allResults));
      }
    }
  }
};

shim.start(new Chaincode());
