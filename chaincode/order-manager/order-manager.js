'use strict';
const shim = require('fabric-shim');
const util = require('util');
const prefix = 'unit_';

let Chaincode = class {

  async Init(stub) {
    console.info('=========== Instantiated OM chaincode ===========');
    return shim.success();
  }

  async Invoke(stub) {
    let ret = stub.getFunctionAndParameters();
    console.info(ret);

    let method = this[ret.fcn];
    if (!method) {
      console.error('no function of name:' + ret.fcn + ' found');
      throw new Error('Received unknown function ' + ret.fcn + ' invocation');
    }
    try {
      let payload = await method(stub, ret.params);
      return shim.success(payload);
    } catch (err) {
      console.log(err);
      return shim.error(err);
    }
  }
  async initLedger(stub, args) {
    console.info('============= START : Initialize Ledger ===========');
    const unitId = prefix + '0';
    const unit = {
      docType: 'unit',
      owner: 'user1',
      brand: 'JackDaniels',
      payload: '100 bottles'
    };

    await stub.putState(unitId, Buffer.from(JSON.stringify(unit)));
    console.info('============= END : Initialize Ledger ===========');
  }

  async createUnit(stub, args) {
    console.info('============= START : Create Unit ===========');
    if (args.length != 4) {
      throw new Error('Incorrect number of arguments. Expecting 4');
    }
    const userName = args[0].toString()
    const unitName = prefix + args[1].toString()
    const brand = args[2].toString();
    const payload = args[3].toString();
    const unitAsBytes = await stub.getState(unitName);
    if (unitAsBytes.toString().length > 0) {
      throw new Error(unitName + ' already exists.');
    }

    const unit = {
      docType: 'unit',
      owner: userName,
      brand: brand,
      payload: payload
    };

    await stub.putState(unitName, Buffer.from(JSON.stringify(unit)));
    console.info('============= END : Create Unit ===========');
  }

  async queryAllUnits(stub, args) {

    let startKey = '';
    let endKey = '';

    let iterator = await stub.getStateByRange(startKey, endKey);

    let allResults = [];
    while (true) {
      let res = await iterator.next();

      if (res.value && res.value.value.toString()) {
        let jsonRes = {};
        console.log(res.value.value.toString('utf8'));

        jsonRes.Key = res.value.key;
        try {
          jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
        } catch (err) {
          console.log(err);
          jsonRes.Record = res.value.value.toString('utf8');
        }
        allResults.push(jsonRes);
      }
      if (res.done) {
        console.log('end of data');
        await iterator.close();
        console.info(allResults);
        return Buffer.from(JSON.stringify(allResults));
      }
    }
  }
};

shim.start(new Chaincode());
