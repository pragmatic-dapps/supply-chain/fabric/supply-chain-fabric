'use strict';
/*
 * Copyright IBM Corp All Rights Reserved
 *
 * SPDX-License-Identifier: Apache-2.0
 */
/*
 * Chaincode Invoke
 */

const Fabric_Client = require('fabric-client');
const path = require('path');
const util = require('util');
const os = require('os');

const fabric_client = new Fabric_Client();

const channel = fabric_client.newChannel('mychannel');
const peer = fabric_client.newPeer('grpc://localhost:7051');
channel.addPeer(peer);
const order = fabric_client.newOrderer('grpc://localhost:7050')
channel.addOrderer(order);

let member_user = null;
const store_path = path.join(__dirname, 'hfc-key-store');
console.log('Store path:'+store_path);
let tx_id = null;
// fromUser - user1
// toUser - user2
//funds - 10
// userName  = toUser
// unitName = i
// brand = JackDaniels;
// payload = "100 bottles";
const testScenario = async (args) => {
  const fromUser = args[0].toString();
  const toUser = args[1].toString();
  const funds = args[2].toString();
  const userName = args[3].toString()
  const unitName = args[4].toString();
  const brand = args[5].toString();
  const payload = args[6].toString();

  await transferReward(fromUser, toUser, funds);
  await createUnit(userName, unitName, brand, payload);
};

const transferReward = async (fromUser, toUser, funds) => {
  await Fabric_Client.newDefaultKeyValueStore({ path: store_path
  }).then((state_store) => {
    fabric_client.setStateStore(state_store);
    const crypto_suite = Fabric_Client.newCryptoSuite();
    const crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    return fabric_client.getUserContext(fromUser, true);
  }).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
      console.log(`Successfully loaded ${fromUser} from persistence`);
      member_user = user_from_store;
    } else {
      throw new Error(`Failed to get ${fromUser}.... run registerUser.js`);
    }
    tx_id = fabric_client.newTransactionID();
    console.log("Assigning transaction_id: ", tx_id._transaction_id);
    const request = {
      //targets: let default to the peer assigned to the client
      chaincodeId: 'supply-chain-token',
      fcn: 'transferFunds',
      args: [fromUser, toUser, funds],
      chainId: 'mychannel',
      txId: tx_id
    };
    return channel.sendTransactionProposal(request);
  }).then((results) => {
    const proposalResponses = results[0];
    const proposal = results[1];
    let isProposalGood = false;
    if (proposalResponses && proposalResponses[0].response &&
      proposalResponses[0].response.status === 200) {
      isProposalGood = true;
      console.log('Transaction proposal was good');
    } else {
      console.error('Transaction proposal was bad');
    }
    if (isProposalGood) {
      console.log(util.format(
        'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
        proposalResponses[0].response.status, proposalResponses[0].response.message));
      const request = {
        proposalResponses: proposalResponses,
        proposal: proposal
      };
      const transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event
      const promises = [];
      const sendPromise = channel.sendTransaction(request);
      promises.push(sendPromise); //we want the send transaction first, so that we know where to check status
      let event_hub = channel.newChannelEventHub(peer);
      let txPromise = new Promise((resolve, reject) => {
        let handle = setTimeout(() => {
          event_hub.unregisterTxEvent(transaction_id_string);
          event_hub.disconnect();
          resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
        }, 3000);
        event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
            clearTimeout(handle);
            const return_status = {event_status : code, tx_id : transaction_id_string};
            if (code !== 'VALID') {
              console.error('The transaction was invalid, code = ' + code);
              resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
            } else {
              console.log('The transaction has been committed on peer ' + event_hub.getPeerAddr());
              resolve(return_status);
            }
          }, (err) => {
            reject(new Error('There was a problem with the eventhub ::'+err));
          },
          {disconnect: true} //disconnect when complete
        );
        event_hub.connect();
      });
      promises.push(txPromise);
      return Promise.all(promises);
    } else {
      console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
      throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    }
  }).then((results) => {
    console.log('Send transaction promise and event listener promise have completed');
    if (results && results[0] && results[0].status === 'SUCCESS') {
      console.log('Successfully sent transaction to the orderer.');
    } else {
      console.error('Failed to order the transaction. Error code: ' + results[0].status);
    }
    if(results && results[1] && results[1].event_status === 'VALID') {
      console.log('Successfully committed the change to the ledger by the peer');
    } else {
      console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
    }
  }).catch((err) => {
    console.error('Failed to invoke successfully :: ' + err);
  });
};

const createUnit = async (userName, unitName, brand, payload) =>{
  await Fabric_Client.newDefaultKeyValueStore({ path: store_path
  }).then((state_store) => {
    fabric_client.setStateStore(state_store);
    const crypto_suite = Fabric_Client.newCryptoSuite();
    const crypto_store = Fabric_Client.newCryptoKeyStore({path: store_path});
    crypto_suite.setCryptoKeyStore(crypto_store);
    fabric_client.setCryptoSuite(crypto_suite);
    return fabric_client.getUserContext(userName, true);
  }).then((user_from_store) => {
    if (user_from_store && user_from_store.isEnrolled()) {
      console.log(`Successfully loaded ${userName} from persistence`);
      member_user = user_from_store;
    } else {
      throw new Error(`Failed to get ${userName}.... run registerUser.js`);
    }
    tx_id = fabric_client.newTransactionID();
    console.log("Assigning transaction_id: ", tx_id._transaction_id);
    const request = {
      chaincodeId: 'order-manager',
      fcn: 'createUnit',
      args: [userName, unitName, brand, payload],
      chainId: 'mychannel',
      txId: tx_id
    };
    return channel.sendTransactionProposal(request);
  }).then((results) => {
    const proposalResponses = results[0];
    const proposal = results[1];
    let isProposalGood = false;
    if (proposalResponses && proposalResponses[0].response &&
      proposalResponses[0].response.status === 200) {
      isProposalGood = true;
      console.log('Transaction proposal was good');
    } else {
      console.error('Transaction proposal was bad');
    }
    if (isProposalGood) {
      console.log(util.format(
        'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
        proposalResponses[0].response.status, proposalResponses[0].response.message));
      const request = {
        proposalResponses: proposalResponses,
        proposal: proposal
      };
      const transaction_id_string = tx_id.getTransactionID(); //Get the transaction ID string to be used by the event
      const promises = [];
      const sendPromise = channel.sendTransaction(request);
      promises.push(sendPromise); //we want the send transaction first, so that we know where to check status
      let event_hub = channel.newChannelEventHub(peer);
      let txPromise = new Promise((resolve, reject) => {
        let handle = setTimeout(() => {
          event_hub.unregisterTxEvent(transaction_id_string);
          event_hub.disconnect();
          resolve({event_status : 'TIMEOUT'}); //we could use reject(new Error('Trnasaction did not complete within 30 seconds'));
        }, 3000);
        event_hub.registerTxEvent(transaction_id_string, (tx, code) => {
            clearTimeout(handle);
            const return_status = {event_status : code, tx_id : transaction_id_string};
            if (code !== 'VALID') {
              console.error('The transaction was invalid, code = ' + code);
              resolve(return_status); // we could use reject(new Error('Problem with the tranaction, event status ::'+code));
            } else {
              console.log('The transaction has been committed on peer ' + event_hub.getPeerAddr());
              resolve(return_status);
            }
          }, (err) => {
            reject(new Error('There was a problem with the eventhub ::'+err));
          },
          {disconnect: true} //disconnect when complete
        );
        event_hub.connect();
      });
      promises.push(txPromise);
      return Promise.all(promises);
    } else {
      console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
      throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
    }
  }).then((results) => {
    console.log('Send transaction promise and event listener promise have completed');
    if (results && results[0] && results[0].status === 'SUCCESS') {
      console.log('Successfully sent transaction to the orderer.');
    } else {
      console.error('Failed to order the transaction. Error code: ' + results[0].status);
    }
    if(results && results[1] && results[1].event_status === 'VALID') {
      console.log('Successfully committed the change to the ledger by the peer');
    } else {
      console.log('Transaction failed to be committed to the ledger due to ::'+results[1].event_status);
    }
  }).catch((err) => {
    console.error('Failed to invoke successfully :: ' + err);
  });
};

module.exports = {
  testScenario
};