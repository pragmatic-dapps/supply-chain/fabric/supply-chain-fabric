'use strict';
/*
 * Copyright IBM Corp All Rights Reserved
 *
 * SPDX-License-Identifier: Apache-2.0
 */
/*
 * Chaincode Invoke
 */

const testScenario = require('./testTransferFundsScenario');

const runScenario = async () => {
  const before = Date.now();
  await testScenario.testScenario();
  const spent = Date.now() - before;
  console.log(spent);
};

// const prepareAndRun = (txNumber) => {
//   for (let i = 1; i <= txNumber; i++) {
//     const args = [];
//     const fromUser = 'user1';
//     const toUser = 'user2';
//     const funds = '1';
//     const userName = 'user2';
//     const unitName = '2'+i;
//     const brand = 'JackDaniels';
//     const payload = '100 bottles';
//     args.push(fromUser);
//     args.push(toUser);
//     args.push(funds);
//     args.push(userName);
//     args.push(unitName);
//     args.push(brand);
//     args.push(payload);
//     testScenario.testScenario(args);
//   }
// };

runScenario();
